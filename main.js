const Twitter = require("twitter");
const config = require("./config");

const client = new Twitter({
  consumer_key: config.consumer_key,
  consumer_secret: config.consumer_secret,
  access_token_key: config.access_token_key,
  access_token_secret: config.access_token_secret
});

async function main() {
  const opts = { screen_name: config.screen_name, count: 200 };
  if (config.max_id) {
    opts.max_id = config.max_id;
  }
  const tweets = await client.get("statuses/user_timeline", opts);
  console.log(`Deleting ${tweets.length} tweets`);
  for (let tweet of tweets) {
    try {
      // console.log(`Deleting ${tweet.id_str}: ${tweet.text}`);
      await client.post("statuses/destroy", { id: tweet.id_str });
    } catch (e) {
      console.error(e);
    }
  }
  console.log("Done");
}

main();
